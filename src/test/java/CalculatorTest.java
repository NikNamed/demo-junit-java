import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testSum1(){
        Calculator calculator = new Calculator();
        double actual = calculator.sum(200.05, 500.01);

        Assert.assertEquals(700.06, actual, 0.001);
    }

}
